import React, { Component } from 'react';
import { View, StyleSheet, Button } from 'react-native'

import {
	SQIPCore,
} from 'react-native-square-in-app-payments';

import {
	SQIPCardEntry,
} from 'react-native-square-in-app-payments';

export default class App extends Component {

	constructor() {
		super();
		// bind 'this' to the methods' context
		this.onStartCardEntry = this.onStartCardEntry.bind(this);
		this.onCardNonceRequestSuccess = this.onCardNonceRequestSuccess.bind(this);
	}

	async componentDidMount() {
		try {
			await SQIPCore.setSquareApplicationId('sq0idp-l8SflSGgJJHs8FM_L79pWA');

		} catch (error) {
			console.error(error);
		}
	}

    /**
     * Callback when the card entry is closed after call 'SQIPCardEntry.completeCardEntry'
     */
	onCardEntryComplete() {
		// Update UI to notify user that the payment flow is completed
		console.log('==> Payment completed');

	}

    /**
     * Callback when successfully get the card nonce details for processig
     * card entry is still open and waiting for processing card nonce details
     * @param {*} cardDetails
     */
	async onCardNonceRequestSuccess(cardDetails) {
		try {
			// take payment with the card details
			// await chargeCard(cardDetails);

			// payment finished successfully
			// you must call this method to close card entry
			await SQIPCardEntry.completeCardEntry(
				this.onCardEntryComplete(),
			);
		} catch (ex) {
			// payment failed to complete due to error
			// notify card entry to show processing error
			await SQIPCardEntry.showCardNonceProcessingError(ex.message);
		}
	}

    /**
     * Callback when card entry is cancelled and UI is closed
     */
	onCardEntryCancel() {
		// Handle the cancel callback
		console.log("==> Payment canceled")

	}

    /**
     * An event listener to start card entry flow
     */
	async onStartCardEntry() {
		const cardEntryConfig = {
			collectPostalCode: false,
		};
		try {
			await SQIPCardEntry.startCardEntryFlow(
				cardEntryConfig,
				this.onCardNonceRequestSuccess,
				this.onCardEntryCancel,
			);

		} catch (error) {
			console.error(error);

		}
	}

	render() {
		return (
			<View style={styles.container}>
				<Button
					onPress={this.onStartCardEntry}
					title="Start Card Entry"
				/>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#fff',
		alignItems: 'center',
		justifyContent: 'center',
	},
});
